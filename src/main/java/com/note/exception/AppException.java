package com.note.exception;

public class AppException extends RuntimeException{

    private String errorType;
    private String errorCode;
    private String message;


    public AppException(String errorType, String errorCode, String message) {
        this.errorType = errorType;
        this.errorCode = errorCode;
        this.message = message;
    }

    public AppException(String message) {
        this.message = message;
    }
}

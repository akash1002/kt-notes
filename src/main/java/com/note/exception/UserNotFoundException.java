package com.note.exception;

public class UserNotFoundException extends AppException{
    public UserNotFoundException(String errorType, String errorCode, String message) {
        super(errorType, errorCode, message);
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}

package com.note.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rating")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Rating extends BaseEntity{
    private float rating;
    private String review;

    private Long ratedTo;
    @ManyToOne
    @JoinColumn(name = "notes_id")
    private Notes notes;
    private Long ratedBy;
}

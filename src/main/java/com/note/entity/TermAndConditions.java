package com.note.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "term_and_condition")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TermAndConditions extends BaseEntity{
    private String description;
}

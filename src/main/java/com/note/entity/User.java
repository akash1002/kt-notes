package com.note.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseEntity{
    private String name;
    private String phoneNumber;
    private String jwtToken;
    private String highestQualification;
    private String experience;
    private String profession;
    private String bio;
    private String email;
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Notes> notes;

    @OneToMany(mappedBy = "user")
    private List<Feedback> feedbacks;

}

package com.note.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "notes_master")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Notes extends BaseEntity{
    private String title;
    private String shortDesc;
    private String subject;
    private String class_standard;
    private String price;
    private String discount;
    private Date uploadedDate;

    private String isPurchased;

    @ManyToOne
    @JoinColumn(name = "userid")
    private User user;

    @OneToMany(mappedBy = "notes")
    private List<Rating> ratings;

}

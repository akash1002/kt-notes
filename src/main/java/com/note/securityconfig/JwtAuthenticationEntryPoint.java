package com.note.securityconfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.note.constants.AppConstants;
import com.note.exception.AppException;
import com.note.response.BaseApiResponse;
import com.note.response.ResponseStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        AppException appException = new AppException(AppConstants.ErrorTypes.INVALID_TOKEN_ERROR_TYPE,
                AppConstants.ErrorCodes.INVALID_TOKEN_ERROR_CODE,
                AppConstants.ErrorMessages.INVALID_TOKEN_ERROR_MESSAGE);
        ResponseStatus stattus=new ResponseStatus();
        stattus.setStatusCode(AppConstants.statusCode.FAILURE);
        baseApiResponse.setResponseStatus(stattus);
        baseApiResponse.setMessage(appException.getMessage());
        httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        ObjectMapper Obj = new ObjectMapper();
        String json = Obj.writeValueAsString(baseApiResponse);
        httpServletResponse.getWriter().write(json);
    }
}

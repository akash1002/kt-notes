package com.note.securityconfig;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserDetailRepo extends JpaRepository<UserDetailEntity,Long> {
    Optional<UserDetailEntity> findByEmail(String email);
}

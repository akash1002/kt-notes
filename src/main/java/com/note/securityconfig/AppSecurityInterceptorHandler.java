package com.note.securityconfig;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AppSecurityInterceptorHandler extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        response.addHeader("Access-Control-Allow-Origin", "*");

        response.addHeader("Access-Control-Allow-Methods", "*");

        response.setHeader("Access-Control-Allow-Headers", "*");

        response.setHeader("Access-Control-Expose-Headers",
                "Content-disposition, Authorization");
        response.addHeader("Access-Control-Max-Age", "3600");
       
        return true;
    }
}

package com.note.securityconfig;

import com.note.constants.AppConstants;
import com.note.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailService implements UserDetailsService {
    @Autowired
    private UserDetailRepo userDetailRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserDetailEntity entity=userDetailRepo.findByEmail(email).orElseThrow(()->
                new AppException(AppConstants.ErrorTypes.ENTITY_DOES_NOT_EXISTS,
                        AppConstants.ErrorCodes.ENTITY_DOES_NOT_EXISTS,
                        AppConstants.ErrorMessages.ENTITY_DOES_NOT_EXISTS));
        return UserDetail.create(entity);
    }
}

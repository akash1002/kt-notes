package com.note.securityconfig;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class UserDetailEntity {
    @Column(name = "id", nullable = false, updatable = false,insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(name="email",nullable = false, length = 100)
    private String email;

    @Column(name ="password",nullable = false, columnDefinition = "TEXT")
    private String password;

    @Column(name = "is_active",columnDefinition = "BOOLEAN")
    private Boolean active;

}

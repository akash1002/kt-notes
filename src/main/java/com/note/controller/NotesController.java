package com.note.controller;

import com.note.constants.RestMappingConstant;
import com.note.response.BaseApiResponse;
import com.note.response.ResponseBuilder;
import com.note.service.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class NotesController {
    @Autowired
    NotesService notesService;

    @PostMapping(path = RestMappingConstant.Notes.GET_NOTES)
    public ResponseEntity<BaseApiResponse> saveNotes(@RequestParam String notes) {
        BaseApiResponse baseApiResponse = ResponseBuilder.getSuccessResponse(notesService.test(notes));
        return new ResponseEntity<BaseApiResponse>(baseApiResponse, HttpStatus.OK);
    }

}

package com.note.advice;

import com.note.constants.AppConstants;
import com.note.exception.UserNotFoundException;
import com.note.response.BaseApiResponse;
import com.note.response.ResponseStatus;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class UniversalAdviceController extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<BaseApiResponse> UserNotFound(UserNotFoundException userNotFoundException, HttpServletRequest request) {
    BaseApiResponse baseApiResponse = new BaseApiResponse();
    baseApiResponse.setResponseData(new ResponseStatus(AppConstants.statusCode.FAILURE));
    baseApiResponse.setResponseData(userNotFoundException);
    return new ResponseEntity<>(baseApiResponse, HttpStatus.OK);
    }

}

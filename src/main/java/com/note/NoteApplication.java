package com.note;

import com.note.constants.AppConstants;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.util.Properties;

@SpringBootApplication
public class NoteApplication extends SpringBootServletInitializer {


    public static void main(String[] args) {
        new SpringApplicationBuilder(NoteApplication.class).sources(NoteApplication.class)
                .properties(getProperties())
                .run(args);
    }

    static Properties getProperties() {
        Properties props = new Properties();
        props.put("spring.config.location", AppConstants.filepath.PROPERTY_PATH);
        return props;
    }

    @Override
    protected SpringApplicationBuilder
    configure(SpringApplicationBuilder springApplicationBuilder) {
        return
                springApplicationBuilder.sources(NoteApplication.class).properties(
                        getProperties());
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}

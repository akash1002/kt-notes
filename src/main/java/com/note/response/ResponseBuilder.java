package com.note.response;

import com.note.constants.AppConstants;
import com.note.exception.AppException;

public class ResponseBuilder {
    public static BaseApiResponse getSuccessResponse(Object responseData) throws AppException {
    BaseApiResponse baseApiResponse = new BaseApiResponse();
    baseApiResponse.setResponseData(AppConstants.statusCode.SUCCESS);
    baseApiResponse.setResponseData(responseData);
    baseApiResponse.setResponseData(new ResponseStatus(AppConstants.statusCode.SUCCESS));
    baseApiResponse.setMessage("Success");
    return baseApiResponse;
    }
}

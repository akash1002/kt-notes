package com.note.response;

public class ResponseStatus {
    private int statusCode;

    public ResponseStatus() {
    }

    public ResponseStatus(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}

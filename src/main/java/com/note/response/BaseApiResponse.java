package com.note.response;

public class BaseApiResponse {

    private ResponseStatus responseStatus;

    private Object responseData;

    private String message;

    public BaseApiResponse() {
    }

    public BaseApiResponse(ResponseStatus responseStatus, Object responseData, String message) {
        this.responseStatus = responseStatus;
        this.responseData = responseData;
        this.message = message;
    }

    public ResponseStatus getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(ResponseStatus responseStatus) {
        this.responseStatus = responseStatus;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(Object responseData) {
        this.responseData = responseData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}

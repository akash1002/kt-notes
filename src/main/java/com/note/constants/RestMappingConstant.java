package com.note.constants;

public interface RestMappingConstant {
    String APP_BASE = "/Notes/v1";

public interface Notes{
    String GET_NOTES = APP_BASE + "/notes";
}
}

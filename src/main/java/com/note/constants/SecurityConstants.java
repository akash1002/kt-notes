package com.note.constants;

public interface SecurityConstants {
    public interface SecretKey {
        String SECRET = "SecretKeyToGenJWTsK1-vedalegal-innovation-k1";
        String TOKEN_PREFIX = "Bearer ";
        String TOKEN_HEADER = "Authorization";

    }

}

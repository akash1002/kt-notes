package com.note.constants;

public interface AppConstants {



    public interface filepath {
        String PROPERTY_PATH = "classpath:application.properties";
    }

    interface statusCode {
        int SUCCESS = 1;
        int FAILURE = 0;
    }

    public interface ErrorTypes {
        String ENTITY_DOES_NOT_EXISTS = "Entity Doesn't Exists";
        String INVALID_TOKEN_ERROR_TYPE = "Invalid Token";
    }

    public interface ErrorCodes {
        String ENTITY_DOES_NOT_EXISTS = "175";
        String INVALID_TOKEN_ERROR_CODE = "1001";
    }

    public interface ErrorMessages {
        String ENTITY_DOES_NOT_EXISTS = "Entity Doesn't Exists";
        String INVALID_TOKEN_ERROR_MESSAGE = "Token Not Valid";
    }
}

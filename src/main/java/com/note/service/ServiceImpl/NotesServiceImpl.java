package com.note.service.ServiceImpl;

import com.note.entity.Notes;
import com.note.repository.NotesRepository;
import com.note.service.NotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotesServiceImpl implements NotesService {

    @Autowired
    NotesRepository notesRepository;

    @Override
    public String test(String notesName) {
        Notes notes = new Notes();
        notes.setTitle(notesName);
        notesRepository.save(notes);
        return "Saved";
    }
}
